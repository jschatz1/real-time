const express = require('express');
const router = express.Router();
const serveStatic = require('serve-static');
const SseStream = require('ssestream');

// a tiny event thing
const ev = {
  _cbs: [],
  on(e, cb) {
    this._cbs.push({
      e,
      cb
    });
  },

  trigger(e) {
    this._cbs.forEach( item => {
      if(item.e === e) {
        item.cb();
      }
    });
  }
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/save', function(req, res, next) {
  ev.trigger('save');
  res.json({ status: 'OK' });
});

router.get('/sse', function(req, res) {
  console.log('new connection');
  const sseStream = new SseStream(req);
  ev.on('save', function() {
    sseStream.write({
      event: 'server-save',
      data: {
        status: 'OK',
        date: new Date().toTimeString()
      }
    })
  });
  sseStream.pipe(res);
  const pusher = setInterval(function() {
    sseStream.write({
      event: 'server-time',
      data: new Date().toTimeString()
    });
  }, 1000)

  res.on('close', function() {
    console.log('lost connection');
    clearInterval(pusher);
    sseStream.unpipe(res);
  });
});

module.exports = router;
