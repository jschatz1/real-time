document.addEventListener('DOMContentLoaded', function(){
  console.log('loaded');
  var es = new EventSource('/sse');
  es.addEventListener('server-time', function (e) {
    console.log('server-time', e.data);
  });

  es.addEventListener('server-save', function (e) {
    console.log('save', e.data);
  });

  setInterval(function(){
    axios.post('/save');
  }, 2000);
})